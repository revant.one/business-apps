import { DynamicModule, Module, Provider } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { defer, lastValueFrom } from 'rxjs';
import {
  AsyncDatabaseOptions,
  AUTH_MODULE_OPTIONS,
  handleRetry,
  IdentityDatabaseOptions,
  IdentityProvider,
  IDENTITY_CONNECTION,
} from './common';
import { IdentityEntities, IdentityEntityServices } from './entities';
import { RoleGuard } from './guards/role.guard';
import { TenantGuard } from './guards/tenant.guard';
import { TokenGuard } from './guards/token.guard';

@Module({
  providers: [
    TokenGuard,
    RoleGuard,
    TenantGuard,
    ...IdentityEntities,
    ...IdentityEntityServices,
  ],
  exports: [TokenGuard, RoleGuard, TenantGuard, ...IdentityEntityServices],
})
export class AuthModule {
  static registerAsync(options: AsyncDatabaseOptions): DynamicModule {
    return {
      module: AuthModule,
      providers: [...this.createAsyncProviders(options)],
    };
  }

  private static createAsyncProviders(
    options: AsyncDatabaseOptions,
  ): Provider[] {
    const providers: Provider[] = [
      {
        provide: IDENTITY_CONNECTION,
        useFactory: async (
          options: IdentityDatabaseOptions,
        ): Promise<typeof mongoose> => {
          const mongoOptions = 'retryWrites=true';
          return await lastValueFrom(
            defer(() =>
              mongoose.connect(
                `${options.mongoUriPrefix}://${options.identityDbUser}:${
                  options.identityDbPassword
                }@${options.identityDbHost.replace(/,\s*$/, '')}/${
                  options.identityDbName
                }?${mongoOptions}`,
                {
                  useNewUrlParser: true,
                  useUnifiedTopology: true,
                  autoReconnect: false,
                  reconnectTries: 0,
                  reconnectInterval: 0,
                  useCreateIndex: true,
                },
              ),
            ).pipe(handleRetry(IdentityProvider)),
          );
        },
        inject: [AUTH_MODULE_OPTIONS],
      },
    ];
    return [this.createAsyncOptionsProvider(options), ...providers];
  }

  private static createAsyncOptionsProvider(
    options: AsyncDatabaseOptions,
  ): Provider {
    return {
      provide: AUTH_MODULE_OPTIONS,
      useFactory: options.createIdDbOptions,
      inject: options.inject,
    };
  }
}

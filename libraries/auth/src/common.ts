import { Logger } from '@nestjs/common';
import { Observable } from 'rxjs';
import { delay, retryWhen, scan } from 'rxjs/operators';

export const IDENTITY_CONNECTION = 'IDENTITY_CONNECTION';
export const IdentityProvider = 'IdentityProvider';
export const AUTH_MODULE_OPTIONS = 'AUTH_MODULE_OPTION';

export interface IdentityDatabaseOptions {
  mongoUriPrefix: string;
  identityDbHost: string;
  identityDbUser: string;
  identityDbPassword: string;
  identityDbName: string;
}

export interface AsyncDatabaseOptions {
  inject: any[];
  createIdDbOptions: (...args) => IdentityDatabaseOptions;
}

export function handleRetry(
  providerName: string,
  retryAttempts = 9,
  retryDelay = 3000,
): <T>(source: Observable<T>) => Observable<T> {
  return <T>(source: Observable<T>) =>
    source.pipe(
      retryWhen(e =>
        e.pipe(
          scan((errorCount, error) => {
            Logger.error(
              error,
              `Unable to connect to the database. Retrying (${
                errorCount + 1
              })...`,
              providerName,
            );
            if (errorCount + 1 >= retryAttempts) {
              throw error;
            }
            return errorCount + 1;
          }, 0),
          delay(retryDelay),
        ),
      ),
    );
}

import { Inject, Injectable } from '@nestjs/common';
import { FilterQuery, InsertManyOptions, Model, QueryOptions } from 'mongoose';
import { Client } from './client.interface';
import { CLIENT } from './client.schema';

@Injectable()
export class ClientService {
  constructor(@Inject(CLIENT) public readonly model: Model<Client>) {}

  public async insertOne(params) {
    return await this.model.create(params);
  }

  public async insertMany(params, options?: InsertManyOptions) {
    return await this.model.insertMany(params, options);
  }

  public async findOne(
    filter?: FilterQuery<Client>,
    projections?,
    options?: QueryOptions,
  ): Promise<Client> {
    return await this.model.findOne(filter, projections, options);
  }

  async find(
    filter?: FilterQuery<Client>,
    projections?,
    options?: QueryOptions,
  ): Promise<Client[]> {
    return await this.model.find(filter, projections, options).exec();
  }

  public async updateOne(
    filter: FilterQuery<Client>,
    update,
    options?: QueryOptions,
  ) {
    return await this.model.updateOne(filter, update, options);
  }

  public async updateMany(
    filter: FilterQuery<Client>,
    update,
    options?: QueryOptions,
  ) {
    return await this.model.updateMany(filter, update, options);
  }

  public async deleteOne(filter: FilterQuery<Client>, options?: QueryOptions) {
    return await this.model.deleteOne(filter, options);
  }

  public async deleteMany(filter: FilterQuery<Client>, options?: QueryOptions) {
    return await this.model.deleteMany(filter, options);
  }
}

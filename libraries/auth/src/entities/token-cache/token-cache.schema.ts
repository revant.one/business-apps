import * as mongoose from 'mongoose';

const schema = new mongoose.Schema(
  {
    uuid: String,
    creation: Date,
    modified: Date,
    createdBy: String,
    modifiedBy: String,
    accessToken: { type: String, index: true, unique: true, sparse: true },
    refreshToken: String,
    redirectUris: [String],
    scope: [String],
    expiresIn: Number,
    user: String,
    client: String,
    isTrustedClient: Boolean,
  },
  { collection: 'token_cache', versionKey: false },
);

export const TokenCache = schema;

export const TOKEN_CACHE = 'TokenCache';

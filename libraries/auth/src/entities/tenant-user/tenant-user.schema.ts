import * as mongoose from 'mongoose';

const schema = new mongoose.Schema(
  {
    tenant: { type: String, index: true },
    user: { type: String, index: true },
  },
  { collection: 'tenant_user', versionKey: false },
);

export const TenantUser = schema;

export const TENANT_USER = 'TenantUser';

import { Document } from 'mongoose';

export interface TenantUser extends Document {
  tenant: string;
  user: string;
}

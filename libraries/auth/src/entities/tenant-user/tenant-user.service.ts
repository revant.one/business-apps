import { Inject, Injectable } from '@nestjs/common';
import { FilterQuery, InsertManyOptions, Model, QueryOptions } from 'mongoose';
import { TenantUser } from './tenant-user.interface';
import { TENANT_USER } from './tenant-user.schema';

@Injectable()
export class TenantUserService {
  constructor(@Inject(TENANT_USER) public readonly model: Model<TenantUser>) {}

  public async insertOne(params) {
    return await this.model.create(params);
  }

  public async insertMany(params, options?: InsertManyOptions) {
    return await this.model.insertMany(params, options);
  }

  public async findOne(
    filter?: FilterQuery<TenantUser>,
    projections?,
    options?: QueryOptions,
  ): Promise<TenantUser> {
    return await this.model.findOne(filter, projections, options);
  }

  async find(
    filter?: FilterQuery<TenantUser>,
    projections?,
    options?: QueryOptions,
  ): Promise<TenantUser[]> {
    return await this.model.find(filter, projections, options).exec();
  }

  public async updateOne(
    filter: FilterQuery<TenantUser>,
    update,
    options?: QueryOptions,
  ) {
    return await this.model.updateOne(filter, update, options);
  }

  public async updateMany(
    filter: FilterQuery<TenantUser>,
    update,
    options?: QueryOptions,
  ) {
    return await this.model.updateMany(filter, update, options);
  }

  public async deleteOne(
    filter: FilterQuery<TenantUser>,
    options?: QueryOptions,
  ) {
    return await this.model.deleteOne(filter, options);
  }

  public async deleteMany(
    filter: FilterQuery<TenantUser>,
    options?: QueryOptions,
  ) {
    return await this.model.deleteMany(filter, options);
  }
}

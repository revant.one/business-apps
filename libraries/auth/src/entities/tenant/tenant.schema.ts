import * as mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

const schema = new mongoose.Schema(
  {
    uuid: { type: String, index: true, default: uuidv4 },
    createdById: { type: String },
    createdByActor: { type: String },
  },
  { collection: 'tenant', versionKey: false },
);

export const Tenant = schema;

export const TENANT = 'Tenant';

import { Provider } from '@nestjs/common';
import { Connection } from 'mongoose';
import { IDENTITY_CONNECTION } from '../common';
import { Client, CLIENT } from './client/client.schema';
import { ClientService } from './client/client.service';
import { TenantUser, TENANT_USER } from './tenant-user/tenant-user.schema';
import { TenantUserService } from './tenant-user/tenant-user.service';
import { Tenant, TENANT } from './tenant/tenant.schema';
import { TenantService } from './tenant/tenant.service';
import { TokenCache, TOKEN_CACHE } from './token-cache/token-cache.schema';
import { TokenCacheService } from './token-cache/token-cache.service';
import { User, USER } from './user/user.schema';
import { UserService } from './user/user.service';

export const IdentityEntities: Provider[] = [
  {
    provide: TOKEN_CACHE,
    useFactory: (connection: Connection) => {
      connection.model(TOKEN_CACHE, TokenCache);
    },
    inject: [IDENTITY_CONNECTION],
  },
  {
    provide: USER,
    useFactory: (connection: Connection) => connection.model(USER, User),
    inject: [IDENTITY_CONNECTION],
  },
  {
    provide: TENANT,
    useFactory: (connection: Connection) => connection.model(TENANT, Tenant),
    inject: [IDENTITY_CONNECTION],
  },
  {
    provide: TENANT_USER,
    useFactory: (connection: Connection) =>
      connection.model(TENANT_USER, TenantUser),
    inject: [IDENTITY_CONNECTION],
  },
  {
    provide: CLIENT,
    useFactory: (connection: Connection) => connection.model(CLIENT, Client),
    inject: [IDENTITY_CONNECTION],
  },
];

export const IdentityEntityServices: Provider[] = [
  TokenCacheService,
  UserService,
  TenantService,
  TenantUserService,
  ClientService,
];

import { DynamicModule } from '@nestjs/common';
import { AsyncDatabaseOptions } from './common';
export declare class AuthModule {
    static registerAsync(options: AsyncDatabaseOptions): DynamicModule;
    private static createAsyncProviders;
    private static createAsyncOptionsProvider;
}

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ClientService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("mongoose");
const client_schema_1 = require("./client.schema");
let ClientService = class ClientService {
    constructor(model) {
        this.model = model;
    }
    async insertOne(params) {
        return await this.model.create(params);
    }
    async insertMany(params, options) {
        return await this.model.insertMany(params, options);
    }
    async findOne(filter, projections, options) {
        return await this.model.findOne(filter, projections, options);
    }
    async find(filter, projections, options) {
        return await this.model.find(filter, projections, options).exec();
    }
    async updateOne(filter, update, options) {
        return await this.model.updateOne(filter, update, options);
    }
    async updateMany(filter, update, options) {
        return await this.model.updateMany(filter, update, options);
    }
    async deleteOne(filter, options) {
        return await this.model.deleteOne(filter, options);
    }
    async deleteMany(filter, options) {
        return await this.model.deleteMany(filter, options);
    }
};
ClientService = __decorate([
    common_1.Injectable(),
    __param(0, common_1.Inject(client_schema_1.CLIENT)),
    __metadata("design:paramtypes", [mongoose_1.Model])
], ClientService);
exports.ClientService = ClientService;
//# sourceMappingURL=client.service.js.map
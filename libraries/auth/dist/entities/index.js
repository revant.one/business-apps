"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.IdentityEntityServices = exports.IdentityEntities = void 0;
const common_1 = require("../common");
const client_schema_1 = require("./client/client.schema");
const client_service_1 = require("./client/client.service");
const tenant_user_schema_1 = require("./tenant-user/tenant-user.schema");
const tenant_user_service_1 = require("./tenant-user/tenant-user.service");
const tenant_schema_1 = require("./tenant/tenant.schema");
const tenant_service_1 = require("./tenant/tenant.service");
const token_cache_schema_1 = require("./token-cache/token-cache.schema");
const token_cache_service_1 = require("./token-cache/token-cache.service");
const user_schema_1 = require("./user/user.schema");
const user_service_1 = require("./user/user.service");
exports.IdentityEntities = [
    {
        provide: token_cache_schema_1.TOKEN_CACHE,
        useFactory: (connection) => {
            connection.model(token_cache_schema_1.TOKEN_CACHE, token_cache_schema_1.TokenCache);
        },
        inject: [common_1.IDENTITY_CONNECTION],
    },
    {
        provide: user_schema_1.USER,
        useFactory: (connection) => connection.model(user_schema_1.USER, user_schema_1.User),
        inject: [common_1.IDENTITY_CONNECTION],
    },
    {
        provide: tenant_schema_1.TENANT,
        useFactory: (connection) => connection.model(tenant_schema_1.TENANT, tenant_schema_1.Tenant),
        inject: [common_1.IDENTITY_CONNECTION],
    },
    {
        provide: tenant_user_schema_1.TENANT_USER,
        useFactory: (connection) => connection.model(tenant_user_schema_1.TENANT_USER, tenant_user_schema_1.TenantUser),
        inject: [common_1.IDENTITY_CONNECTION],
    },
    {
        provide: client_schema_1.CLIENT,
        useFactory: (connection) => connection.model(client_schema_1.CLIENT, client_schema_1.Client),
        inject: [common_1.IDENTITY_CONNECTION],
    },
];
exports.IdentityEntityServices = [
    token_cache_service_1.TokenCacheService,
    user_service_1.UserService,
    tenant_service_1.TenantService,
    tenant_user_service_1.TenantUserService,
    client_service_1.ClientService,
];
//# sourceMappingURL=index.js.map
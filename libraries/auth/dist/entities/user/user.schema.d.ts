import * as mongoose from 'mongoose';
export declare const User: mongoose.Schema<mongoose.Document<any, any, any>, mongoose.Model<any, any, any>, undefined, any>;
export declare const USER = "User";
export declare const UserModel: mongoose.Model<unknown, {}, {}>;

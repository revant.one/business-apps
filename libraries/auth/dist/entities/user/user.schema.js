"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserModel = exports.USER = exports.User = void 0;
const mongoose = require("mongoose");
const schema = new mongoose.Schema({
    uuid: { type: String, index: true, unique: true, sparse: true },
    creation: { type: Date },
    modified: Date,
    createdBy: String,
    modifiedBy: String,
    disabled: { type: Boolean, default: false },
    name: String,
    phone: { type: String, unique: true, sparse: true, index: true },
    unverifiedPhone: String,
    email: { type: String, unique: true, sparse: true, index: true },
    roles: [String],
    enable2fa: { type: Boolean, default: false },
    deleted: { type: Boolean, default: false },
    enablePasswordLess: { type: Boolean, default: false },
    isEmailVerified: { type: Boolean, default: false },
}, { collection: 'token_user', versionKey: false });
exports.User = schema;
exports.USER = 'User';
exports.UserModel = mongoose.model(exports.USER, exports.User);
//# sourceMappingURL=user.schema.js.map
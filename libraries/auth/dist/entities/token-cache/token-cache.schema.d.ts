import * as mongoose from 'mongoose';
export declare const TokenCache: mongoose.Schema<mongoose.Document<any, any, any>, mongoose.Model<any, any, any>, undefined, any>;
export declare const TOKEN_CACHE = "TokenCache";

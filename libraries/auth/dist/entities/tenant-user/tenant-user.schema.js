"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TENANT_USER = exports.TenantUser = void 0;
const mongoose = require("mongoose");
const schema = new mongoose.Schema({
    tenant: { type: String, index: true },
    user: { type: String, index: true },
}, { collection: 'tenant_user', versionKey: false });
exports.TenantUser = schema;
exports.TENANT_USER = 'TenantUser';
//# sourceMappingURL=tenant-user.schema.js.map
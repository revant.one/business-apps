import { FilterQuery, InsertManyOptions, Model, QueryOptions } from 'mongoose';
import { Tenant } from './tenant.interface';
export declare class TenantService {
    readonly model: Model<Tenant>;
    constructor(model: Model<Tenant>);
    insertOne(params: any): Promise<Tenant>;
    insertMany(params: any, options?: InsertManyOptions): Promise<Tenant[]>;
    findOne(filter?: FilterQuery<Tenant>, projections?: any, options?: QueryOptions): Promise<Tenant>;
    find(filter?: FilterQuery<Tenant>, projections?: any, options?: QueryOptions): Promise<Tenant[]>;
    updateOne(filter: FilterQuery<Tenant>, update: any, options?: QueryOptions): Promise<import("mongoose").UpdateWriteOpResult>;
    updateMany(filter: FilterQuery<Tenant>, update: any, options?: QueryOptions): Promise<import("mongoose").UpdateWriteOpResult>;
    deleteOne(filter: FilterQuery<Tenant>, options?: QueryOptions): Promise<{
        ok?: number;
        n?: number;
    } & {
        deletedCount?: number;
    }>;
    deleteMany(filter: FilterQuery<Tenant>, options?: QueryOptions): Promise<{
        ok?: number;
        n?: number;
    } & {
        deletedCount?: number;
    }>;
}

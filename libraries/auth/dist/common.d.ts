import { Observable } from 'rxjs';
export declare const IDENTITY_CONNECTION = "IDENTITY_CONNECTION";
export declare const IdentityProvider = "IdentityProvider";
export declare const AUTH_MODULE_OPTIONS = "AUTH_MODULE_OPTION";
export interface IdentityDatabaseOptions {
    mongoUriPrefix: string;
    identityDbHost: string;
    identityDbUser: string;
    identityDbPassword: string;
    identityDbName: string;
}
export interface AsyncDatabaseOptions {
    inject: any[];
    createIdDbOptions: (...args: any[]) => IdentityDatabaseOptions;
}
export declare function handleRetry(providerName: string, retryAttempts?: number, retryDelay?: number): <T>(source: Observable<T>) => Observable<T>;

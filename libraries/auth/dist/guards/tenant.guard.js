"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TenantGuard = void 0;
const common_1 = require("@nestjs/common");
const operators_1 = require("rxjs/operators");
const rxjs_1 = require("rxjs");
const tenant_user_service_1 = require("../entities/tenant-user/tenant-user.service");
let TenantGuard = class TenantGuard {
    constructor(tenantUser) {
        this.tenantUser = tenantUser;
    }
    canActivate(context) {
        var _a;
        const httpContext = context.switchToHttp();
        const req = httpContext.getRequest();
        return rxjs_1.from(this.tenantUser.find({ user: (_a = req.token) === null || _a === void 0 ? void 0 : _a.user })).pipe(operators_1.map(tenantUsers => {
            if ((tenantUsers === null || tenantUsers === void 0 ? void 0 : tenantUsers.length) > 0) {
                req.tenants = tenantUsers.map(user => user.tenant);
            }
            return true;
        }));
    }
};
TenantGuard = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [tenant_user_service_1.TenantUserService])
], TenantGuard);
exports.TenantGuard = TenantGuard;
//# sourceMappingURL=tenant.guard.js.map
import { CanActivate, ExecutionContext } from '@nestjs/common';
import { TenantUserService } from '../entities/tenant-user/tenant-user.service';
export declare class TenantGuard implements CanActivate {
    private readonly tenantUser;
    constructor(tenantUser: TenantUserService);
    canActivate(context: ExecutionContext): import("rxjs").Observable<boolean>;
}

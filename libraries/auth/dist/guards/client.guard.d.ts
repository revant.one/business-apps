import { CanActivate, ExecutionContext } from '@nestjs/common';
import { ClientService } from '../entities/client/client.service';
export declare class ClientGuard implements CanActivate {
    private readonly client;
    constructor(client: ClientService);
    canActivate(context: ExecutionContext): import("rxjs").Observable<boolean>;
}

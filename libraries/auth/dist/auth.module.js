"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var AuthModule_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose = require("mongoose");
const rxjs_1 = require("rxjs");
const common_2 = require("./common");
const entities_1 = require("./entities");
const role_guard_1 = require("./guards/role.guard");
const tenant_guard_1 = require("./guards/tenant.guard");
const token_guard_1 = require("./guards/token.guard");
let AuthModule = AuthModule_1 = class AuthModule {
    static registerAsync(options) {
        return {
            module: AuthModule_1,
            providers: [...this.createAsyncProviders(options)],
        };
    }
    static createAsyncProviders(options) {
        const providers = [
            {
                provide: common_2.IDENTITY_CONNECTION,
                useFactory: async (options) => {
                    const mongoOptions = 'retryWrites=true';
                    return await rxjs_1.lastValueFrom(rxjs_1.defer(() => mongoose.connect(`${options.mongoUriPrefix}://${options.identityDbUser}:${options.identityDbPassword}@${options.identityDbHost.replace(/,\s*$/, '')}/${options.identityDbName}?${mongoOptions}`, {
                        useNewUrlParser: true,
                        useUnifiedTopology: true,
                        autoReconnect: false,
                        reconnectTries: 0,
                        reconnectInterval: 0,
                        useCreateIndex: true,
                    })).pipe(common_2.handleRetry(common_2.IdentityProvider)));
                },
                inject: [common_2.AUTH_MODULE_OPTIONS],
            },
        ];
        return [this.createAsyncOptionsProvider(options), ...providers];
    }
    static createAsyncOptionsProvider(options) {
        return {
            provide: common_2.AUTH_MODULE_OPTIONS,
            useFactory: options.createIdDbOptions,
            inject: options.inject,
        };
    }
};
AuthModule = AuthModule_1 = __decorate([
    common_1.Module({
        providers: [
            token_guard_1.TokenGuard,
            role_guard_1.RoleGuard,
            tenant_guard_1.TenantGuard,
            ...entities_1.IdentityEntities,
            ...entities_1.IdentityEntityServices,
        ],
        exports: [token_guard_1.TokenGuard, role_guard_1.RoleGuard, tenant_guard_1.TenantGuard, ...entities_1.IdentityEntityServices],
    })
], AuthModule);
exports.AuthModule = AuthModule;
//# sourceMappingURL=auth.module.js.map
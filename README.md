# Business Apps

Monorepo of business apps

## Prerequisites

- NodeJS
- yarn
- lerna
- docker

## Development

```shell
git clone https://gitlab.com/castlecraft/business-apps && cd business-apps
yarn && lerna bootstrap
```

Start containers for MongoDB and MQTT

```shell
docker-compose --project-name ba -f docker/docker-compose-events.yml -f docker/docker-compose-mongo.yml up -d
```

To add db/user to MongoDB

```shell
mongo tokendb \
  --host localhost \
  --port 27017 \
  -u root \
  -p admin \
  --authenticationDatabase admin \
  --eval "db.createUser({user: 'tokenuser', pwd: 'admin', roles:[{role:'dbOwner', db: 'tokendb'}], passwordDigestor: 'server'});"
```

Note:

- You may need to change db name `tokendb`, db user `tokenuser`, and db password `admin`.
- Add token_user and token_cache to mongodb. Refer `user.interface.ts` and `token-cache.interface.ts` for keys.

Samples:

token_user:

```json
{
  "uuid": "86a8bc85-5cd8-435c-9c69-be98f8c3dfb0",
  "creation": "2021-07-13T11:04:17.913Z",
  "modified": "2021-07-13T11:04:17.913Z",
  "disabled": false,
  "name": "Test User",
  "phone": "+919876543210",
  "email": "test@user.org",
  "roles": ["administrator"],
  "enable2fa": false,
  "enablePasswordLess": false,
  "isEmailVerified": true,
}
```

token_cache:

```json
{
  "creation": "Date",
  "modified": "Date",
  "createdBy": "string",
  "modifiedBy": "string",
  "accessToken": "string",
  "refreshToken": "string",
  "redirectUris": ["string"],
  "scope": ["string"],
  "expiresIn": 2000000000, // set life-long expiry
  "user": "86a8bc85-5cd8-435c-9c69-be98f8c3dfb0", // same as user's uuid
  "client": "f3ee5e72-f46c-4d00-80cf-ee16cc6e0625",
  "isTrustedClient": true,
}
```

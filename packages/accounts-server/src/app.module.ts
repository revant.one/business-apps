import { AuthModule } from '@castlecraft/auth';
import { HttpModule, Module } from '@nestjs/common';
import { AccountsModule } from './accounts/accounts.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CommonModule } from './common/common.module';
import { ConfigModule } from './config/config.module';
import {
  ConfigService,
  ID_DB_HOST,
  ID_DB_NAME,
  ID_DB_PASSWORD,
  ID_DB_USER,
  MONGO_URI_PREFIX,
} from './config/config.service';

@Module({
  imports: [
    HttpModule,
    ConfigModule,
    CommonModule,
    AccountsModule,
    AuthModule.registerAsync({
      createIdDbOptions: (config: ConfigService) => ({
        mongoUriPrefix: config.get(MONGO_URI_PREFIX) || 'mongodb',
        identityDbHost: config.get(ID_DB_HOST),
        identityDbUser: config.get(ID_DB_USER),
        identityDbPassword: config.get(ID_DB_PASSWORD),
        identityDbName: config.get(ID_DB_NAME),
      }),
      inject: [ConfigService],
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

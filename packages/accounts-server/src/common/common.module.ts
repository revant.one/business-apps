import { Global, Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { ClientsModule } from '@nestjs/microservices';
import { DatabaseProvider } from '../database.provider';
import { CommonCommandHandlers } from './commands';
import { eventsClient } from './events-microservice.client';
import { CommonSagas } from './sagas';

@Global()
@Module({
  imports: [ClientsModule.registerAsync([eventsClient]), CqrsModule],
  providers: [...CommonCommandHandlers, ...CommonSagas, DatabaseProvider],
  exports: [DatabaseProvider, CqrsModule],
})
export class CommonModule {}

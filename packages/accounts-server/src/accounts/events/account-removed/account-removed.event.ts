import { IEvent } from '@nestjs/cqrs';
import { Account } from '../../entities/account/account-entity';

export class AccountRemovedEvent implements IEvent {
  constructor(public account: Account) {}
}

import { IEvent } from '@nestjs/cqrs';
import { Account } from '../../entities/account/account-entity';

export class AccountAddedEvent implements IEvent {
  constructor(public account: Account) {}
}

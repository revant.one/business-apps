import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { AccountService } from '../../entities/account/account.service';
import { AccountUpdatedEvent } from './account-updated.event';

@EventsHandler(AccountUpdatedEvent)
export class AccountUpdatedHandler
  implements IEventHandler<AccountUpdatedEvent>
{
  constructor(private readonly object: AccountService) {}

  async handle(event: AccountUpdatedEvent) {
    const { updatePayload } = event;
    await this.object.updateOne(
      { uuid: updatePayload.uuid },
      { $set: updatePayload },
    );
  }
}

import { IEvent } from '@nestjs/cqrs';
import { AccountDto } from '../../entities/account/account-dto';

export class AccountUpdatedEvent implements IEvent {
  constructor(public updatePayload: AccountDto) {}
}

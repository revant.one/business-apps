import { Module } from '@nestjs/common';
import { AccountEntityServices, AccountEntities } from './entities';
import { AccountsAggregateService } from './aggregates/accounts-aggregate/accounts-aggregate.service';
import { JournalEntryAggregateService } from './aggregates/journal-entry-aggregate/journal-entry-aggregate.service';
import { AccountController } from './controllers/account/account.controller';
import { AccountsCommandManager } from './command';
import { AccountEventManager } from './events';
import { AccountQueryManager } from './query';

@Module({
  providers: [
    ...AccountEntities,
    ...AccountEntityServices,
    ...AccountsCommandManager,
    ...AccountEventManager,
    ...AccountQueryManager,
    AccountsAggregateService,
    JournalEntryAggregateService,
  ],
  controllers: [AccountController],
  exports: [...AccountEntityServices],
})
export class AccountsModule {}

import { ICommand } from '@nestjs/cqrs';
import { AccountDto } from '../../entities/account/account-dto';

export class UpdateAccountCommand implements ICommand {
  constructor(public readonly updatePayload: AccountDto) {}
}

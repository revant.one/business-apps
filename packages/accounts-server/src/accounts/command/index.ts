import { AddAccountHandler } from './add-account/add-account.handler';
import { RemoveAccountHandler } from './remove-account/remove-account.handler';
import { UpdateAccountHandler } from './update-account/update-account.handler';

export const AccountsCommandManager = [
  AddAccountHandler,
  RemoveAccountHandler,
  UpdateAccountHandler,
];

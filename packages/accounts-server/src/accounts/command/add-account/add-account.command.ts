import { ICommand } from '@nestjs/cqrs';
import { AccountDto } from '../../entities/account/account-dto';

export class AddAccountCommand implements ICommand {
  constructor(public accountPayload: AccountDto) {}
}

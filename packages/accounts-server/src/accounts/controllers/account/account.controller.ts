import {
  Controller,
  Req,
  Param,
  Get,
  Query,
  Body,
  Post,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { RemoveAccountCommand } from '../../command/remove-account/remove-account.command';
import { AddAccountCommand } from '../../command/add-account/add-account.command';
import { AccountDto } from '../../entities/account/account-dto';
import { RetrieveAccountListQuery } from '../../query/list-account/retrieve-account-list.query';
import { UpdateAccountCommand } from '../../command/update-account/update-account.command';

@Controller('account')
export class AccountController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/create')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async createAccount(@Body() accountPayload: AccountDto, @Req() req) {
    return await this.commandBus.execute(new AddAccountCommand(accountPayload));
  }

  @Get('v1/list')
  async getAccountList(
    @Query('offset') offset = 0,
    @Query('limit') limit = 10,
    @Query('search') search = '',
    @Query('sort') sort,
  ) {
    try {
      search = decodeURIComponent(search);
    } catch {}
    return await this.queryBus.execute(
      new RetrieveAccountListQuery(offset, limit, sort, search),
    );
  }

  @Post('v1/remove/:uuid')
  removeAccount(@Param('uuid') uuid) {
    return this.commandBus.execute(new RemoveAccountCommand(uuid));
  }

  @Post('v1/update')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async updateAccount(@Body() updatePayload: AccountDto, @Req() req) {
    return await this.commandBus.execute(
      new UpdateAccountCommand(updatePayload),
    );
  }
}

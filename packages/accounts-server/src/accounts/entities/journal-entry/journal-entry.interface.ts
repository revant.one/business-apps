import { Document } from 'mongoose';

export interface JournalEntry extends Document {
  uuid: string;
  tenantId: string;
}

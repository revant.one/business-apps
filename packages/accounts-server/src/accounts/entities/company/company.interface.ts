import { Document } from 'mongoose';

export interface Company extends Document {
  uuid: string;
  tenantId: string;
  children: string[];
}

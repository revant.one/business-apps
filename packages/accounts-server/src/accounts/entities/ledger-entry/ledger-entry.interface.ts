import { Document } from 'mongoose';

export interface LedgerEntry extends Document {
  uuid: string;
  tenantId: string;
  postingDate: Date;
  transactionDate: Date;
  account: string;
  debitAmount: number;
  creditAmount: number;
  accountCurrency: string;
  debitInAccountCurrency: number;
  creditInAccountCurrency: number;
  journalEntry: string;
  remarks: string;
  company: string;
}

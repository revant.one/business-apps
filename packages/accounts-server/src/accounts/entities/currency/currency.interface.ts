import { Document } from 'mongoose';

export interface Currency extends Document {
  name: string;
  symbol: string;
  code: string;
  country: string;
  fraction: string;
  fractionUnits: number;
  smallestFraction: number;
  numberFormat: NumberFormat;
}

export enum NumberFormat {
  '#,###.##' = '#,###.##',
  '#.###,##' = '#.###,##',
  '# ###.##' = '# ###.##',
  '# ###,##' = '# ###,##',
  "#'###.##" = "#'###.##",
  '#, ###.##' = '#, ###.##',
  '#,##,###.##' = '#,##,###.##',
  '#,###.###' = '#,###.###',
  '#.###' = '#.###',
  '#,###' = '#,###',
}

import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class AccountDto {
  @IsOptional()
  @IsString()
  uuid: string;

  @IsNotEmpty()
  @IsString()
  tenantId: string;

  @IsOptional()
  children: string[];

  @IsOptional()
  @IsString()
  company: string;

  @IsOptional()
  @IsString()
  currency: string;

  @IsOptional()
  @IsString()
  parentAccount: string;

  @IsOptional()
  @IsString()
  accountName: string;

  @IsOptional()
  @IsString()
  accountNumber: string;
}

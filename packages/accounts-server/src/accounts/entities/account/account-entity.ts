export class Account {
  uuid: string;
  tenantId: string;
  children?: string[];
  company: string;
  currency: string;
  parentAccount: string;
  accountName: string;
  accountNumber: string;
}

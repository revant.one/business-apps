import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { PARSE_REGEX } from '../../../constants/app-strings';
import { Account } from './account-entity';
import { ACCOUNT } from './account.schema';

@Injectable()
export class AccountService {
  constructor(
    @Inject(ACCOUNT)
    public readonly model: Model<Account>,
  ) {}

  async create(account: Account) {
    return await this.model.create(account);
  }

  async list(skip, take, sort, filter_query) {
    const results = await this.model.find();
    return {
      docs: results || [],
      length: await this.model.countDocuments(filter_query),
      offset: skip,
    };
  }

  async findOne(query) {
    return await this.model.findOne(query);
  }

  async deleteOne(query) {
    return await this.model.deleteOne(query);
  }

  async updateOne(query, param) {
    return await this.model.updateOne(query, param);
  }

  getFilterQuery(query) {
    const keys = Object.keys(query);
    keys.forEach(key => {
      if (typeof query[key] === 'string') {
        query[key] = { $regex: PARSE_REGEX(query[key]), $options: 'i' };
      } else {
        delete query[key];
      }
      if (typeof query[key] !== 'undefined') {
      } else {
        delete query[key];
      }
    });
    return query;
  }
}

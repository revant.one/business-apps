import { Test, TestingModule } from '@nestjs/testing';
import { JournalEntryAggregateService } from './journal-entry-aggregate.service';

describe('JournalEntryAggregateService', () => {
  let service: JournalEntryAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [JournalEntryAggregateService],
    }).compile();

    service = module.get<JournalEntryAggregateService>(
      JournalEntryAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

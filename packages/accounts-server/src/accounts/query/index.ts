import { RetrieveAccountListHandler } from './list-account/retrieve-account-list.handler';

export const AccountQueryManager = [RetrieveAccountListHandler];

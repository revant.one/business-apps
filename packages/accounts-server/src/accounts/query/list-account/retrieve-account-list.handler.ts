import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { AccountsAggregateService } from '../../aggregates/accounts-aggregate/accounts-aggregate.service';
import { RetrieveAccountListQuery } from './retrieve-account-list.query';

@QueryHandler(RetrieveAccountListQuery)
export class RetrieveAccountListHandler
  implements IQueryHandler<RetrieveAccountListQuery>
{
  constructor(private readonly manager: AccountsAggregateService) {}
  async execute(query: RetrieveAccountListQuery) {
    const { offset, limit, search, sort } = query;
    return await this.manager.getAccountsList(
      Number(offset),
      Number(limit),
      search,
      sort,
    );
  }
}

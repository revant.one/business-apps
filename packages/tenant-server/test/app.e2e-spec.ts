import {
  ClientService,
  TokenCacheService,
  UserService,
} from '@castlecraft/auth';
import { INestApplication } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { Test } from '@nestjs/testing';
import * as request from 'supertest';
import { TenantController } from '../src/controllers/tenant/tenant.controller';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [CqrsModule],
      controllers: [TenantController],
      providers: [
        { provide: TokenCacheService, useValue: {} },
        { provide: UserService, useValue: {} },
        { provide: ClientService, useValue: {} },
      ],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/ (GET)', done => {
    return request(app.getHttpServer()).get('/').end(done);
  });

  afterAll(async () => {
    await app.close();
  });
});

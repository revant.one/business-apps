FROM node:latest
# Copy app
COPY . /home/craft/tenant-server
WORKDIR /home/craft/
RUN npm config set @castlecraft:registry https://gitlab.com/api/v4/projects/28015527/packages/npm/ \
    && cd tenant-server \
    && yarn \
    && yarn build \
    && yarn --production=true

FROM node:slim
# Install dependencies
RUN apt-get update \
    && apt-get install -y gettext-base \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Setup docker-entrypoint
COPY docker/docker-entrypoint.sh usr/local/bin/docker-entrypoint.sh
RUN ln -s usr/local/bin/docker-entrypoint.sh / # backwards compat

# Add non root user
RUN useradd -ms /bin/bash craft
WORKDIR /home/craft/tenant-server
COPY --from=0 /home/craft/tenant-server .

RUN chown -R craft:craft /home/craft

# set project directory
WORKDIR /home/craft/tenant-server

# Expose port
EXPOSE 8000

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["start"]

import {
  ClientService,
  TokenCacheService,
  UserService,
} from '@castlecraft/auth';
import { CqrsModule } from '@nestjs/cqrs';
import { Test, TestingModule } from '@nestjs/testing';
import { TenantController } from './tenant.controller';

describe('TenantController', () => {
  let controller: TenantController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [CqrsModule],
      controllers: [TenantController],
      providers: [
        { provide: TokenCacheService, useValue: {} },
        { provide: UserService, useValue: {} },
        { provide: ClientService, useValue: {} },
      ],
    }).compile();

    controller = module.get<TenantController>(TenantController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

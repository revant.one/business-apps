import {
  ClientGuard,
  CreatedByActor,
  RoleGuard,
  Roles,
  TokenGuard,
  UserGuard,
} from '@castlecraft/auth';
import {
  Body,
  Controller,
  Get,
  Post,
  Query,
  Req,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { AddTenantUserCommand } from '../../commands/add-tenant-user/add-tenant-user.command';
import { AddTenantCommand } from '../../commands/add-tenant/add-tenant.command';
import { RemoveTenantUserCommand } from '../../commands/remove-tenant-user/remove-tenant-user.command';
import { RemoveTenantCommand } from '../../commands/remove-tenant/remove-tenant.command';
import { ADMINISTRATOR } from '../../constants/strings';
import { ListTenantUserQuery } from '../../queries/list-tenant-user/list-tenant-user.query';
import { ListTenantQuery } from '../../queries/list-tenant/list-tenant.query';
import { TenantUserDto } from './tenant-user.dto';
import { TenantDto } from './tenant.dto';

@Controller('tenant')
export class TenantController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/create_tenant_by_admin')
  @Roles(ADMINISTRATOR)
  @UseGuards(TokenGuard, UserGuard, RoleGuard)
  async createTenantByAdmin(@Req() req) {
    return await this.commandBus.execute(
      new AddTenantCommand(CreatedByActor.User, req?.user?.uuid),
    );
  }

  @Post('v1/create_tenant_by_client')
  @UseGuards(TokenGuard, ClientGuard)
  async createTenantByClient(@Req() req) {
    return await this.commandBus.execute(
      new AddTenantCommand(CreatedByActor.Client, req?.client?.clientId),
    );
  }

  @Post('v1/add_tenant_user_by_admin')
  @Roles(ADMINISTRATOR)
  @UseGuards(TokenGuard, UserGuard, RoleGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async addTenantUserByAdmin(@Req() req, @Body() body: TenantUserDto) {
    return await this.commandBus.execute(
      new AddTenantUserCommand(
        body.tenantId,
        body.userUuid,
        CreatedByActor.User,
        req?.user?.uuid,
      ),
    );
  }

  @Post('v1/create_tenant_user_by_client')
  @UseGuards(TokenGuard, ClientGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async addTenantUserByClient(@Req() req, @Body() body: TenantUserDto) {
    return await this.commandBus.execute(
      new AddTenantUserCommand(
        body.tenantId,
        body.userUuid,
        CreatedByActor.Client,
        req?.client?.clientId,
      ),
    );
  }

  @Post('v1/remove_tenant_by_admin')
  @Roles(ADMINISTRATOR)
  @UseGuards(TokenGuard, UserGuard, RoleGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async removeTenantByAdmin(@Req() req, @Body() body: TenantDto) {
    return await this.commandBus.execute(
      new RemoveTenantCommand(
        body.tenantId,
        CreatedByActor.User,
        req?.user?.uuid,
      ),
    );
  }

  @Post('v1/remove_tenant_by_client')
  @UseGuards(TokenGuard, ClientGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async removeTenantByClient(@Req() req, @Body() body: TenantDto) {
    return await this.commandBus.execute(
      new RemoveTenantCommand(
        body.tenantId,
        CreatedByActor.Client,
        req?.client?.clientId,
      ),
    );
  }

  @Post('v1/remove_tenant_user_by_admin')
  @Roles(ADMINISTRATOR)
  @UseGuards(TokenGuard, UserGuard, RoleGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async removeTenantUserByAdmin(@Req() req, @Body() body: TenantUserDto) {
    return await this.commandBus.execute(
      new RemoveTenantUserCommand(
        body.tenantId,
        body.userUuid,
        CreatedByActor.User,
        req?.user?.uuid,
      ),
    );
  }

  @Post('v1/remove_tenant_user_by_client')
  @UseGuards(TokenGuard, ClientGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async removeTenantUserByClient(@Req() req, @Body() body: TenantUserDto) {
    return await this.commandBus.execute(
      new RemoveTenantUserCommand(
        body.tenantId,
        body.userUuid,
        CreatedByActor.Client,
        req?.client?.clientId,
      ),
    );
  }

  @Get('v1/list_for_admin')
  @Roles(ADMINISTRATOR)
  @UseGuards(TokenGuard, UserGuard, RoleGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async listTenantsForAdmin(@Req() req, @Query() query: ListTenantQuery) {
    return await this.queryBus.execute(
      new ListTenantQuery(
        query.tenantId,
        query.offset,
        query.limit,
        CreatedByActor.User,
        req?.user?.uuid,
      ),
    );
  }

  @Get('v1/user/list_for_admin')
  @Roles(ADMINISTRATOR)
  @UseGuards(TokenGuard, UserGuard, RoleGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async listTenantUsersForAdmin(@Req() req, @Query() query: ListTenantQuery) {
    return await this.queryBus.execute(
      new ListTenantUserQuery(
        query.tenantId,
        query.offset,
        query.limit,
        CreatedByActor.User,
        req?.user?.uuid,
      ),
    );
  }

  @Get('v1/list_for_client')
  @UseGuards(TokenGuard, ClientGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async listTenantsForClient(@Req() req, @Query() query: ListTenantQuery) {
    return await this.queryBus.execute(
      new ListTenantQuery(
        query.tenantId,
        query.offset,
        query.limit,
        CreatedByActor.Client,
        req?.client?.clientId,
      ),
    );
  }

  @Get('v1/user/list_for_client')
  @UseGuards(TokenGuard, ClientGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async listTenantUsersForClient(@Req() req, @Query() query: ListTenantQuery) {
    return await this.queryBus.execute(
      new ListTenantUserQuery(
        query.tenantId,
        query.offset,
        query.limit,
        CreatedByActor.Client,
        req?.client?.clientId,
      ),
    );
  }
}

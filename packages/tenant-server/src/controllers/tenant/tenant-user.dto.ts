import { IsUUID } from 'class-validator';

export class TenantUserDto {
  @IsUUID()
  userUuid: string;
  @IsUUID()
  tenantId: string;
}

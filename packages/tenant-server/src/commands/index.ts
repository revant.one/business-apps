import { AddTenantUserHandler } from './add-tenant-user/add-tenant-user.handler';
import { AddTenantHandler } from './add-tenant/add-tenant.handler';
import { RemoveTenantUserHandler } from './remove-tenant-user/remove-tenant-user.handler';
import { RemoveTenantHandler } from './remove-tenant/remove-tenant.handler';

export const TenantCommandHandlers = [
  AddTenantHandler,
  AddTenantUserHandler,
  RemoveTenantHandler,
  RemoveTenantUserHandler,
];

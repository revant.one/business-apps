import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { TenantAggregateService } from '../../aggregates/tenant-aggregate/tenant-aggregate.service';
import { RemoveTenantCommand } from './remove-tenant.command';

@CommandHandler(RemoveTenantCommand)
export class RemoveTenantHandler
  implements ICommandHandler<RemoveTenantCommand>
{
  constructor(
    private readonly manager: TenantAggregateService,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: RemoveTenantCommand) {
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const removedTenant = await aggregate.removeTenant(
      command.tenantId,
      command.actorType,
      command.actorUuid,
    );
    aggregate.commit();
    return removedTenant;
  }
}

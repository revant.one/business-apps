import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { TenantAggregateService } from '../../aggregates/tenant-aggregate/tenant-aggregate.service';
import { AddTenantCommand } from './add-tenant.command';

@CommandHandler(AddTenantCommand)
export class AddTenantHandler implements ICommandHandler<AddTenantCommand> {
  constructor(
    private readonly manager: TenantAggregateService,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: AddTenantCommand) {
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const tenant = await aggregate.addTenant(
      command.actorType,
      command.actorUuid,
    );
    aggregate.commit();
    return tenant;
  }
}

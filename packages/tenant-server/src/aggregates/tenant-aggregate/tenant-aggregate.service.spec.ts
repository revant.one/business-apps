import {
  TenantService,
  TenantUserService,
  UserService,
} from '@castlecraft/auth';
import { Test, TestingModule } from '@nestjs/testing';
import { ActorService } from '../../policies/actor/actor.service';
import { TenantAggregateService } from './tenant-aggregate.service';

describe('TenantAggregateService', () => {
  let service: TenantAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TenantAggregateService,
        { provide: ActorService, useValue: {} },
        { provide: TenantService, useValue: {} },
        { provide: TenantUserService, useValue: {} },
        { provide: UserService, useValue: {} },
      ],
    }).compile();

    service = module.get<TenantAggregateService>(TenantAggregateService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

import {
  CreatedByActor,
  Tenant,
  TenantService,
  TenantUser,
  TenantUserService,
  UserService,
} from '@castlecraft/auth';
import { Injectable, NotFoundException } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { v4 as uuidv4 } from 'uuid';
import { TenantAddedEvent } from '../../events/tenant-added/tenant-added.event';
import { TenantRemovedEvent } from '../../events/tenant-removed/tenant-removed.event';
import { TenantUserAddedEvent } from '../../events/tenant-user-added/tenant-user-added.event';
import { TenantUserRemovedEvent } from '../../events/tenant-user-removed/tenant-user-removed.event';
import { ActorService } from '../../policies/actor/actor.service';

@Injectable()
export class TenantAggregateService extends AggregateRoot {
  constructor(
    private readonly actorPolicy: ActorService,
    private readonly tenant: TenantService,
    private readonly tenantUser: TenantUserService,
    private readonly user: UserService,
  ) {
    super();
  }

  async addTenant(actorType: CreatedByActor, actorUuid: string) {
    const actor = await this.actorPolicy.validateActor(actorType, actorUuid);

    const tenant = {} as Tenant;
    tenant.uuid = uuidv4();
    tenant.createdByActor = actorType;
    tenant.createdById = actorUuid;

    this.apply(new TenantAddedEvent(tenant, actor));

    return tenant;
  }

  async removeTenant(
    tenantId: string,
    actorType: CreatedByActor,
    actorUuid: string,
  ) {
    const actor = await this.actorPolicy.validateActor(actorType, actorUuid);
    const tenant = await this.tenant.findOne({ uuid: tenantId });
    if (!tenant) {
      throw new NotFoundException({ tenantId });
    }
    this.apply(new TenantRemovedEvent(tenant, actor));
  }

  async addTenantUser(
    tenantId: string,
    userUuid: string,
    actorType: CreatedByActor,
    actorUuid: string,
  ) {
    const actor = await this.actorPolicy.validateActor(actorType, actorUuid);

    const tenant = await this.tenant.findOne({ uuid: tenantId });
    if (!tenant) {
      throw new NotFoundException({ tenantId });
    }

    const user = await this.user.findOne({ uuid: userUuid });
    if (!user) {
      throw new NotFoundException({ userUuid });
    }

    const tenantUser = {} as TenantUser;
    tenantUser.tenant = tenantId;
    tenantUser.user = userUuid;

    this.apply(new TenantUserAddedEvent(tenantUser, actor));

    return tenantUser;
  }

  async removeTenantUser(
    tenantId: string,
    userUuid: string,
    actorType: CreatedByActor,
    actorUuid: string,
  ) {
    const actor = await this.actorPolicy.validateActor(actorType, actorUuid);

    const tenant = await this.tenant.findOne({ uuid: tenantId });
    if (!tenant) {
      throw new NotFoundException({ tenantId });
    }

    const user = await this.user.findOne({ uuid: userUuid });
    if (!user) {
      throw new NotFoundException({ userUuid });
    }

    const tenantUser = {} as TenantUser;
    tenantUser.tenant = tenantId;
    tenantUser.user = userUuid;

    this.apply(new TenantUserRemovedEvent(tenantUser, actor));
  }

  async listTenants(
    actorType: CreatedByActor,
    actorUuid: string,
    tenantId?: string,
    offset: number = 0,
    limit: number = 10,
  ) {
    await this.actorPolicy.validateActor(actorType, actorUuid);

    const query: { uuid?: string } = {};
    if (tenantId) {
      query.uuid = tenantId;
    }
    const data = this.tenant.model
      .find(query)
      .skip(Number(offset))
      .limit(Number(limit));

    return {
      docs: await data.exec(),
      length: await this.tenant.model.countDocuments(query),
      offset: Number(offset),
    };
  }

  async listTenantUsers(
    actorType: CreatedByActor,
    actorUuid: string,
    tenantId: string,
    offset: number = 0,
    limit: number = 10,
  ) {
    await this.actorPolicy.validateActor(actorType, actorUuid);

    const query: { tenant?: string } = {};
    if (tenantId) {
      query.tenant = tenantId;
    }
    const data = this.tenantUser.model
      .find(query)
      .skip(Number(offset))
      .limit(Number(limit));

    return {
      docs: await data.exec(),
      length: await this.tenantUser.model.countDocuments(query),
      offset: Number(offset),
    };
  }
}

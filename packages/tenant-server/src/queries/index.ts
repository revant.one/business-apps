import { ListTenantUserHandler } from './list-tenant-user/list-tenant-user.handler';
import { ListTenantHandler } from './list-tenant/list-tenant.handler';

export const TenantQueryHandlers = [ListTenantHandler, ListTenantUserHandler];

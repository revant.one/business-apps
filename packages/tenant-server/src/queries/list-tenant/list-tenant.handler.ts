import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { TenantAggregateService } from '../../aggregates/tenant-aggregate/tenant-aggregate.service';
import { ListTenantQuery } from './list-tenant.query';

@QueryHandler(ListTenantQuery)
export class ListTenantHandler implements IQueryHandler<ListTenantQuery> {
  constructor(private readonly manager: TenantAggregateService) {}
  async execute(query: ListTenantQuery) {
    return await this.manager.listTenants(
      query.actorType,
      query.actorUuid,
      query.tenantId,
      query.offset,
      query.limit,
    );
  }
}

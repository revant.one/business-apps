import { IQuery } from '@nestjs/cqrs';
import { CreatedByActor } from '@castlecraft/auth';

export class ListTenantUserQuery implements IQuery {
  constructor(
    public readonly tenantId: string,
    public readonly offset: number,
    public readonly limit: number,
    public readonly actorType: CreatedByActor,
    public readonly actorUuid: string,
  ) {}
}

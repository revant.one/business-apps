import { INestApplication, Logger } from '@nestjs/common';
import {
  MicroserviceOptions,
  MqttOptions,
  Transport,
} from '@nestjs/microservices';
import {
  ConfigService,
  EVENTS_HOST,
  EVENTS_PORT,
  EVENTS_PROTO,
  EVENTS_USER,
  EVENTS_PASSWORD,
} from './config/config.service';

export const eventsConnectionFactory = (config: ConfigService): MqttOptions => {
  const url = `${config.get(EVENTS_PROTO)}://${config.get(
    EVENTS_USER,
  )}:${config.get(EVENTS_PASSWORD)}@${config.get(EVENTS_HOST)}:${config.get(
    EVENTS_PORT,
  )}`;

  return {
    transport: Transport.MQTT,
    options: { url },
  };
};
const LISTENING_TO_EVENTS = 'Listening to events using MQTT';

export const RETRY_ATTEMPTS = 3;
export const RETRY_DELAY = 10;

export function setupEvents(app: INestApplication) {
  const config = app.get<ConfigService>(ConfigService);
  if (config.get(EVENTS_HOST) && config.get(EVENTS_PORT)) {
    const events = app.connectMicroservice<MicroserviceOptions>(
      eventsConnectionFactory(config),
    );
    events.listen(() =>
      Logger.log(LISTENING_TO_EVENTS, events.constructor.name),
    );
  }
}

import { NestFactory } from '@nestjs/core';
import { ExpressAdapter } from '@nestjs/platform-express';
import * as express from 'express';
import { AppModule } from './app.module';
import { GLOBAL_API_PREFIX } from './constants/strings';
import { setupEvents } from './events-server';
import { setupSwagger } from './swagger';

async function bootstrap() {
  const server = new ExpressAdapter(express());
  const app = await NestFactory.create(AppModule, server);
  app.enableCors();
  app.setGlobalPrefix(GLOBAL_API_PREFIX);
  setupSwagger(app);
  setupEvents(app);
  await app.listen(7000);
}
bootstrap();

import {
  Client,
  ClientService,
  CreatedByActor,
  User,
  UserService,
} from '@castlecraft/auth';
import {
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';

@Injectable()
export class ActorService {
  constructor(
    private readonly client: ClientService,
    private readonly user: UserService,
  ) {}

  async validateActor(
    actorType: CreatedByActor,
    actorUuid: string,
  ): Promise<User | Client> {
    if (actorType === CreatedByActor.Client) {
      const client = await this.client.findOne({ clientId: actorUuid });
      if (!client) {
        throw new NotFoundException({ clientId: actorUuid });
      }
      if (!client.isTrusted) {
        throw new UnauthorizedException({ UnTrustedClient: actorUuid });
      }
      return client;
    } else if (actorType === CreatedByActor.User) {
      const user = await this.user.findOne({ uuid: actorUuid });
      if (!user) {
        throw new NotFoundException({ user: actorUuid });
      }
      return user;
    }
  }
}

import { AuthModule } from '@castlecraft/auth';
import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { TenantAggregates } from './aggregates';
import { TenantCommandHandlers } from './commands';
import { ConfigModule } from './config/config.module';
import {
  ConfigService,
  DB_HOST,
  DB_NAME,
  DB_PASSWORD,
  DB_USER,
  MONGO_URI_PREFIX,
} from './config/config.service';
import { TenantControllers } from './controllers';
import { TenantEventHandlers } from './events';
import { TenantPolicies } from './policies';
import { TenantQueryHandlers } from './queries';

@Module({
  imports: [
    ConfigModule,
    AuthModule.registerAsync({
      createIdDbOptions: (config: ConfigService) => ({
        mongoUriPrefix: config.get(MONGO_URI_PREFIX) || 'mongodb',
        identityDbHost: config.get(DB_HOST),
        identityDbUser: config.get(DB_USER),
        identityDbPassword: config.get(DB_PASSWORD),
        identityDbName: config.get(DB_NAME),
      }),
      inject: [ConfigService],
    }),
    CqrsModule,
  ],
  controllers: [...TenantControllers],
  providers: [
    ...TenantAggregates,
    ...TenantPolicies,
    ...TenantCommandHandlers,
    ...TenantQueryHandlers,
    ...TenantEventHandlers,
  ],
})
export class AppModule {}

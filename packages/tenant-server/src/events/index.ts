import { TenantAddedHandler } from './tenant-added/tenant-added.handler';
import { TenantRemovedHandler } from './tenant-removed/tenant-removed.handler';
import { TenantUserAddedHandler } from './tenant-user-added/tenant-user-added.handler';
import { TenantUserRemovedHandler } from './tenant-user-removed/tenant-user-removed.handler';

export const TenantEventHandlers = [
  TenantAddedHandler,
  TenantRemovedHandler,
  TenantUserAddedHandler,
  TenantUserRemovedHandler,
];

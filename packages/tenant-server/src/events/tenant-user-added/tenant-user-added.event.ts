import { Client, TenantUser, User } from '@castlecraft/auth';
import { IEvent } from '@nestjs/cqrs';

export class TenantUserAddedEvent implements IEvent {
  constructor(
    public readonly tenantUser: TenantUser,
    public readonly actor: User | Client,
  ) {}
}

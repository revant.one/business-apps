export const GLOBAL_API_PREFIX = 'api';
export const APP_NAME = 'tenant-server';
export const SWAGGER_ROUTE = 'api-docs';
export const ADMINISTRATOR = 'administrator';

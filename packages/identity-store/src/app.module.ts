import { Module } from '@nestjs/common';
import { AuthModule } from '@castlecraft/auth';
import { ConfigModule } from './config/config.module';
import {
  ConfigService,
  DB_HOST,
  DB_NAME,
  DB_PASSWORD,
  DB_USER,
  MONGO_URI_PREFIX,
} from './config/config.service';
import { Controllers } from './controllers';

@Module({
  imports: [
    ConfigModule,
    AuthModule.registerAsync({
      createIdDbOptions: (config: ConfigService) => ({
        mongoUriPrefix: config.get(MONGO_URI_PREFIX) || 'mongodb',
        identityDbHost: config.get(DB_HOST),
        identityDbUser: config.get(DB_USER),
        identityDbPassword: config.get(DB_PASSWORD),
        identityDbName: config.get(DB_NAME),
      }),
      inject: [ConfigService],
    }),
  ],
  controllers: [...Controllers],
})
export class AppModule {}

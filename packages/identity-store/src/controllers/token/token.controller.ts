import { TokenCacheService } from '@castlecraft/auth';
import { Controller } from '@nestjs/common';
import { EventPattern } from '@nestjs/microservices';
import { EventPayload } from '../../interfaces';

export const BearerTokenAddedEvent = 'BearerTokenAddedEvent';
export const BearerTokenRemovedEvent = 'BearerTokenRemovedEvent';
export const BearerTokensDeletedEvent = 'BearerTokensDeletedEvent';

@Controller('token')
export class TokenController {
  constructor(private readonly token: TokenCacheService) {}

  @EventPattern(BearerTokenAddedEvent)
  bearerTokenAdded(payload: EventPayload) {
    if (payload?.eventData?.token) {
      this.token
        .insertOne(payload?.eventData?.token)
        .then(saved => {})
        .catch(err => {});
    }
  }

  @EventPattern(BearerTokenRemovedEvent)
  bearerTokenRemoved(payload: EventPayload) {
    if (payload?.eventData?.token) {
      this.token
        .deleteMany({ accessToken: payload?.eventData?.token.accessToken })
        .then(saved => {})
        .catch(err => {});
    }
  }

  @EventPattern(BearerTokensDeletedEvent)
  bearerTokensDeleted(payload: EventPayload) {
    if (payload) {
      this.token
        .deleteMany({})
        .then(saved => {})
        .catch(err => {});
    }
  }
}
